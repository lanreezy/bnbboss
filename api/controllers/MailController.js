/**
 * Mailbox Controller
 */

module.exports = {

	/**
     * @apiDefine MailData
     * @apiSuccess {Object} response variable                holding response
     * @apiSuccess {String} response.message                 response message
     * @apiSuccess {Object} response.data                    variable holding response data
     * @apiSuccess {String} response.data.username 			 username of receiver (one of the reserved emails)
     * @apiSuccess {String} response.data.from 				 email address of sender
     * @apiSuccess {String} response.data.subject 			 subject of mail
     * @apiSuccess {String} response.data.message 			 html version of message 
     * @apiSuccess {String} response.data.messageText		 text version of message 
     * @apiSuccess {String} response.data.replyTo 			 the email to send reply to
     * @apiSuccess {Boolean} response.data.read 		     flag to tell if message has been read or not
     */
    
    /**
     * @apiDefine MailDataMulti
     * @apiSuccess {Object} response variable                holding response
     * @apiSuccess {String} response.message                 response message
     * @apiSuccess {Object[]} response.data                  variable holding response data
     * @apiSuccess {String} response.data.username 			 username of receiver (one of the reserved emails)
     * @apiSuccess {String} response.data.from 				 email address of sender
     * @apiSuccess {String} response.data.subject 			 subject of mail
     * @apiSuccess {String} response.data.message 			 html version of message 
     * @apiSuccess {String} response.data.messageText		 text version of message 
     * @apiSuccess {String} response.data.replyTo 			 the email to send reply to
     * @apiSuccess {Boolean} response.data.read 		     flag to tell if message has been read or not
     */
    
    /**
     * @api {post} /mails Create a mail
     * @apiName Create Mail
     * @apiGroup Mail
     * @apiVersion 0.0.1
     *
     * @apiSuccess {String} username 			 username of receiver (one of the reserved emails)
     * @apiSuccess {String} from 				 email address of sender
     * @apiSuccess {String} subject 			 subject of mail
     * @apiSuccess {String} message 			 html version of message 
     * @apiSuccess {String} messageText		 	 text version of message 
     * @apiSuccess {String} replyTo 			 the email to send reply to
     * @apiSuccess {Boolean} read 		     	 flag to tell if message has been read or not
     *
     * @apiUse MailData
     *
     * @apiSuccessExample Success-Response
     * HTTP/1.1 200 OK
     * {
     * 		"response": {
     * 			"message": "Mail created successfully.",
     * 	 		"data": {
     * 	  			"username": "johnny",
     * 	  			"from": "ken@gmail.com",
     * 	  			"subject": "How are you",
     * 	  			"message": "Can you stop?",
     * 	  			"read": false,
     * 	  			"isDeleted": false,
     * 	  			"createdAt": "2016-02-29T16:36:09.780Z",
     * 	  			"updatedAt": "2016-02-29T16:36:09.780Z",
     * 	  			"id": "56d473799960225a4d16b31b"
     * 	         }
     * 	      }
     * 	  }
     * }
     *
     * @apiUse ValidationErrorExample
     */
	create: function(req, res) {
		var data = req.body;
		Mail.create(data).then(function createCB(createdMail) {
            return ResponseService.json(200, res, 'Mail created successfully.', createdMail);
        }).catch(function(err) {
            return ValidationService.jsonResolveError(err, Mail, res);
        });
	},

	/**
     * @api {get} /mails/:id Fetch mail by id
     * @apiName Fetch Mail
     * @apiGroup Mail
     * @apiVersion 0.0.1
     *
     * @apiParam {String} id      mail id
     *
     * @apiUse MailData
     * 
     * @apiSuccessExample Success-Response
     * HTTP/1.1 200 OK
     * {
     * 		"response": {
     * 			"message": "Mail retrieved successfully.",
     * 	 		"data": {
     * 	  			"username": "johnny",
     * 	  			"from": "ken@gmail.com",
     * 	  			"subject": "How are you",
     * 	  			"message": "Can you stop?",
     * 	  			"read": false,
     * 	  			"isDeleted": false,
     * 	  			"createdAt": "2016-02-29T16:36:09.780Z",
     * 	  			"updatedAt": "2016-02-29T16:36:09.780Z",
     * 	  			"id": "56d473799960225a4d16b31b"
     * 	      }
     * 	  }
     * }
     * 
     * @apiUse NotFoundExample
     */
	read: function (req, res) {
		var id = req.params.id;
        Mail.findOne({ id: id, isDeleted: false }).populateAll({ isDeleted: false }).exec(function(err, mail) {
        	if (err) return ValidationService.jsonResolveError(err, Mail, res);
            if (!mail)  return ResponseService.json(404, res, 'Mail not found');
            return ResponseService.json(200, res, 'Mail retrieved successfully', mail);
        })
	},

	/**
     * @api {get} /mails Fetch all mails
     * @apiName Fetch All Mails
     * @apiGroup Mail
     * @apiVersion 0.0.1
     *
     * @apiUse MailData
     * @apiParam {Boolean} read 	   optional. filter by read status (true or false)
     * @apiParam {Number} perPage  	   optional. Number of page to return 
     * @apiParam {Number} skip 		   optional. Number of pages to skip
     *
     * @apiSuccessExample Success-Response
     * HTTP/1.1 200 OK
     * {
     *    "response": {
     *       "message": "Mails retrieved successfully",
     *       "data": {
     *          [
     *             {
     *                "username": "johnny",
     *                "from": "ken@gmail.com",
     *                "subject": "How are you",
     *                "message": "Can you stop?",
     *                "read": false,
     *                "isDeleted": false,
     *                "createdAt": "2016-02-29T16:36:09.780Z",
     *                "updatedAt": "2016-02-29T16:36:09.780Z",
     *                "id": "56d473799960225a4d16b31b"
     *              },
     *              {
     *                "username": "johnny",
     *                "from": "ken@gmail.com",
     *                "subject": "How are you",
     *                "message": "Can you stop?",
     *                "read": false,
     *                "isDeleted": false,
     *                "createdAt": "2016-02-29T16:36:09.780Z",
     *                "updatedAt": "2016-02-29T16:36:09.780Z",
     *                "id": "56d473799960225a4d16b31b"
     *              }   
     *          ]
     *       }
     *    }
     * }
     *
     * @apiUse MailDataMulti
     */
	list: function (req, res) {

		var where = { isDeleted: false };

		if (req.query.read) 
			where.read = (req.query.read == "false") ? false : true;

		Mail.count(where).then(function(counter) {
            var perPage = req.query.perPage || 50;
            var skip = req.query.skip || 0;
            var foundMails = Mail.find(where).limit(perPage).skip(skip);
            return [Promise.resolve(foundMails), counter, perPage, skip];
        }).spread(function(foundMails, counter, perPage, skip) {
            var meta = {
                total: counter,
                skipped: skip,
                perPage: perPage
            };
            return ResponseService.json(200, res, 'Mails retrieved successfully', foundMails, meta);
        }).catch(function(err) {
            return ValidationService.jsonResolveError(err, Mail, res);
        });
	},

	/**
     * @api {put} /mails/:id Update a mail
     * @apiName Update Mail
     * @apiGroup Mail
     * @apiVersion 0.0.1
     *
     * @apiParam {String} response.data.username 			 username of receiver (one of the reserved emails)
     * @apiParam {String} response.data.from 				 email address of sender
     * @apiParam {String} response.data.subject 			 subject of mail
     * @apiParam {String} response.data.message 			 html version of message 
     * @apiParam {String} response.data.messageText		 	 text version of message 
     * @apiParam {String} response.data.replyTo 			 the email to send reply to
     * @apiParam {Boolean} response.data.read 		     	 flag to tell if message has been read or not
     *
     * @apiUse MailData
     *
     * @apiSuccessExample Success-Response
     * {
     * 		"response": {
     * 			"message": "Mail updated successfully.",
     * 	 		"data": {
     * 	    		"username": "johnny",
     *              "from": "ken@gmail.com",
     *              "subject": "How are you",
     *              "message": "Can you stop?",
     *              "read": false,
     *              "isDeleted": false,
     *              "createdAt": "2016-02-29T16:36:09.780Z",
     *              "updatedAt": "2016-02-29T16:36:09.780Z",
     *              "id": "56d473799960225a4d16b31b"
     * 	         }
     * 	      }
     * 	  }
     * }
     *
     * @apiUse ValidationErrorExample
     * @apiUse NotFoundExample
     */
	update: function(req, res) {
        var data = req.body;
        Mail.update({ id: req.param('id') }, data).then(function(updatedMail) {
            if (!updatedMail.length) {
                return ResponseService.json(404, res, "Mail not found");
            }
            return ResponseService.json(200, res, "Updated mail successfully", updatedMail[0]);
        }).catch(function(err) {
            return ValidationService.jsonResolveError(err, Mail, res);
        });
    },

    /**
     * @api {delete} /mails/:id Remove mail
     * @apiName Remove Mail
     * @apiGroup Mail
     * @apiVersion 0.0.1
     *
     * @apiParam {String} id      client's id
     *
     * @apiUse MailData
     *
     * @apiSuccessExample Success-Response
     * HTTP/1.1 200 OK
     * {
     *    "response": {
     *       "message": "Mail removed successfully",
     *    }
     * }
     *
     * @apiUse NotFoundExample
     */
    delete: function (req, res) {
		Mail.findOne({ id: req.param('id'), isDeleted: false }).then(function(existingMail){
            if (!existingMail) {
                return ResponseService.json(404, res, "Mail not found");
            } 
            existingMail.isDeleted = true;
            existingMail.save(function(err, updatedClient){
                if (err) ValidationService.jsonResolveError(err, Mail, res);
                return ResponseService.json(200, res, "Mail removed successfully");
            });
        }).catch(function(err) {
            return ValidationService.jsonResolveError(err, Mail, res);
        });
	}

}