/**
 * User Controller
 *
 * @description :: Server-side logic for managing users
 */
var promise = require('bluebird'),
    bcrypt = require('bcrypt');

module.exports = {

    /**
     * @apiDefine UserData
     * @apiSuccess {Object} response variable                holding response
     * @apiSuccess {String} response.message                 response message
     * @apiSuccess {Object} response.data                    variable holding response data
     * @apiSuccess {String} response.data.firstName          first name of user
     * @apiSuccess {String} response.data.lastLast           last name of user
     * @apiSuccess {String} response.data.email              email address of user
     * @apiSuccess {String} response.data.role               user role (user or admin)
     * @apiSuccess {String} response.data.conciergeName      concierge name
     * @apiSuccess {integer} response.data.numListing         number of listings to add
     */
    
    /**
     * @apiDefine UserDataMulti
     * @apiSuccess {Object} response variable                holding response
     * @apiSuccess {String} response.message                 response message
     * @apiSuccess {Object[]} response.data                  variable holding response data
     * @apiSuccess {String} response.data.firstName          first name of user
     * @apiSuccess {String} response.data.lastLast           last name of user
     * @apiSuccess {String} response.data.email              email address of user
     * @apiSuccess {String} response.data.role               user role (user or admin)
     * @apiSuccess {String} response.data.conciergeName      concierge name
     * @apiSuccess {integer} response.data.numListing         number of listings to add
     */
    
    /**
     * @api {post} /users Create user
     * @apiName Create User
     * @apiGroup User
     * @apiVersion 0.0.1
     *
     * @apiParam {String} firstName                         user first name
     * @apiParam {String} lastName                          user last name
     * @apiParam {String} email                             user email address
     * @apiParam {String} password                          user password
     * @apiParam {String} role                              user role
     * @apiParam {String} conciergeName                     concierge name
     * @apiParam {interger} numListing                      number of listing
     *
     * @apiUse UserData
     *
     * @apiSuccessExample Success-Response
     * HTTP/1.1 200 OK
     * {
     *    "response": {
     *       "message": "User created successfully.",
     *       "data": {
     *          "firstName": "James",
     *          "lastName": "PeterSide",
     *          "email": "james8020@gmail.com",
     *          "role": "user",
     *          "conciergeName": "PeopleBNB",
     *          "numListing": 10,
     *          "isDeleted": false,
     *          "createdAt": "2016-02-11T16:26:55.031Z",
     *          "updatedAt": "2016-02-11T16:26:55.031Z",
     *          "id": 9
     *       }
     *    }
     * }
     *
     * @apiUse ValidationErrorExample
     */
    create: function(req, res) {

        var data = req.body;
        data.email = data.email || ""

        // check if email previously existed
        User.findOneByEmail(data.email).then(function(user){

            if (user) {
                return ValidationService.uniquenessError("email", User, res);
            }

            // create new user
            User.create(data).then(function createCB(createdUser) {
                return ResponseService.json(200, res, 'User created successfully.', createdUser);
            }).catch(function(err) {
                return ValidationService.jsonResolveError(err, User, res);
            });

        }).catch(function(err) {
            return ValidationService.jsonResolveError(err, User, res);
        });
    },



    /**
     * @api {get} /users/:id Fetch user
     * @apiName Fetch User
     * @apiGroup User
     * @apiVersion 0.0.1
     *
     * @apiParam {String} id      user's id
     *
     * @apiUse UserData
     *
     * @apiSuccessExample Success-Response
     * HTTP/1.1 200 OK
     * {
     *    "response": {
     *       "message": "User created successfully.",
     *       "data": {
     *          "firstName": "James",
     *          "lastName": "PeterSide",
     *          "email": "james8020@gmail.com",
     *          "role": "user",
     *          "conciergeName": "PeopleBNB",
     *          "numListing": 10,
     *          "isDeleted": false,
     *          "createdAt": "2016-02-11T16:26:55.031Z",
     *          "updatedAt": "2016-02-11T16:26:55.031Z",
     *          "id": 9
     *       }
     *    }
     * }
     *
     * @apiUse NotFoundExample
     */
    read: function(req, res) {
        var id = req.params.id;
        User.findOne({ id: id, isDeleted: false }).populateAll({ isDeleted: false }).then(function(user) {
            if (!user) {
                return ResponseService.json(404, res, 'User not found');
            }
            return ResponseService.json(200, res, 'User retrieved successfully', user);
        }).catch(function(err) {
            return ValidationService.jsonResolveError(err, User, res);
        });
    },

    /**
     * @api {get} /users Fetch all users
     * @apiName Fetch All Users
     * @apiGroup User
     * @apiVersion 0.0.1
     *
     * @apiUse UserData
     *
     * @apiSuccessExample Success-Response
     * HTTP/1.1 200 OK
     * {
     *    "response": {
     *       "message": "Users retrieved successfully",
     *       "data": {
     *          [
     *             {
     *                "firstName": "James",
     *                "lastName": "PeterSide",
     *                "email": "james8020@gmail.com",
     *                "role": "user",
     *                "conciergeName": "PeopleBNB",
     *                "numListing": 10,
     *                "isDeleted": false,
     *                "createdAt": "2016-02-11T16:26:55.031Z",
     *                "updatedAt": "2016-02-11T16:26:55.031Z",
     *                "id": 9
     *             },
     *             {
     *                "firstName": "James",
     *                "lastName": "PeterSide",
     *                "email": "james8020@gmail.com",
     *                "role": "user",
     *                "conciergeName": "PeopleBNB",
     *                "numListing": 10,
     *                "isDeleted": false,
     *                "createdAt": "2016-02-11T16:26:55.031Z",
     *                "updatedAt": "2016-02-11T16:26:55.031Z",
     *                "id": 9
     *             }
     *          ]
     *       }
     *    }
     * }
     *
     * @apiUse UserDataMulti
     */

    list: function(req, res) {
        User.count().then(function(counter) {
            var perPage = req.query.perPage || 50;
            var skip = req.query.skip || 0;
            var foundUsers = User.find({
                isDeleted: false
            }).populateAll({
                isDeleted: false
            }).limit(perPage).skip(skip);
            return [promise.resolve(foundUsers), counter, perPage, skip];
        }).spread(function(foundUsers, counter, perPage, skip) {
            var meta = {
                total: counter,
                skipped: skip,
                perPage: perPage
            };
            return ResponseService.json(200, res, 'Users retrieved successfully', foundUsers, meta);
        }).catch(function(err) {
            return ValidationService.jsonResolveError(err, User, res);
        });
    },


    /**
     * @api {put} /users/:id Update user
     * @apiName Update User
     * @apiGroup User
     * @apiVersion 0.0.1
     *
     * @apiParam {String} id             user's id
     * @apiParam {String} firstname      user's first name
     * @apiParam {String} lastname       user's last name
     * @apiParam {String} email          user's email address
     * @apiParam {String} conciergeName  name of concierge
     * @apiParam {integer} numListing    number of listing to add
     * @apiParam {String} isDeleted      user's deleted status
     *
     * @apiUse UserData
     *
     * @apiSuccessExample Success-Response
     * HTTP/1.1 200 OK
     * {
     *    "response": {
     *       "message": "User updated successfully",
     *       "data": {
     *          "firstName": "James",
     *          "lastName": "PeterSide",
     *          "email": "james8020@gmail.com",
     *          "role": "user",
     *          "conciergeName": "PeopleBNB",
     *          "numListing": 10,
     *          "isDeleted": false,
     *          "createdAt": "2016-02-11T16:26:55.031Z",
     *          "updatedAt": "2016-02-11T16:26:55.031Z",
     *          "id": 9
     *       }
     *    }
     * }
     *
     * @apiUse ValidationErrorExample
     * @apiUse NotFoundExample
     */
    update: function(req, res) {
        var data = req.body;
        User.update({
            id: req.param('id')
        }, data).then(function(updatedUser) {
            if (!updatedUser.length) {
                return ResponseService.json(404, res, "User not found");
            }
            return ResponseService.json(200, res, "Updated user successfully", updatedUser[0]);
        }).catch(function(err) {
            return ValidationService.jsonResolveError(err, User, res);
        });
    },

    /**
     * @api {delete} /users/:id Remove user
     * @apiName Remove User
     * @apiGroup User
     * @apiVersion 0.0.1
     *
     * @apiParam {String} id      user's id
     *
     * @apiUse UserData
     *
     * @apiSuccessExample Success-Response
     * HTTP/1.1 200 OK
     * {
     *    "response": {
     *       "message": "User removed successfully",
     *    }
     * }
     *
     * @apiUse NotFoundExample
     */
    delete: function(req, res) {
        User.findOne({ id: req.param('id'), isDeleted: false }).then(function(existingUser){
            if (!existingUser) {
                return ResponseService.json(404, res, "User not found");
            } 
            existingUser.isDeleted = true;
            existingUser.save(function(err, updatedUser){
                if (err) ValidationService.jsonResolveError(err, User, res);
                return ResponseService.json(200, res, "User removed successfully");
            });
        }).catch(function(err) {
            return ValidationService.jsonResolveError(err, User, res);
        });
    },

    /**
     * @api {get} /admin/setup Sets up default admin user. 
     * @apiDescription Creates the default admin user. The admin email and password can be set in settings.js in the config directory.
     * @apiName Add Default Admin User
     * @apiGroup User
     * @apiVersion 0.0.1
     *
     * @apiUse UserData
     *
     * @apiSuccessExample Success-Response
     * HTTP/1.1 200 OK
     * {
     *    "response": {
     *       "message": "User updated successfully",
     *       "data": {
     *          "firstName": "James",
     *          "lastName": "PeterSide",
     *          "email": "admin@gmail.com",
     *          "role": "admin",
     *          "conciergeName": "PeopleBNB",
     *          "numListing": 10,
     *          "isDeleted": false,
     *          "createdAt": "2016-02-11T16:26:55.031Z",
     *          "updatedAt": "2016-02-11T16:26:55.031Z",
     *          "id": 1
     *       }
     *    }
     * }
     *
     * @apiUse ValidationErrorExample
     */
    setup: function(req, res) {
        if (process.env.NODE_ENV && process.env.NODE_ENV.toLowerCase() !== "production") {
            var email = sails.config.settings.adminEmail;
            var password = sails.config.settings.adminPassword;
            User.findOne({ email: email, isDeleted: false }).then(function(existingUser){
                if (existingUser) {
                    return ResponseService.json(200, res, 'User created successfully', existingUser.toJSON());
                }

                var adminData = {
                    firstName: "Admin",
                    lastName: "Admin",
                    email: email,
                    password: password,
                    role: "admin"
                }

                User.create(adminData).then(function(newAdmin){
                    return ResponseService.json(200, res, 'User created successfully', newAdmin.toJSON());
                }).catch(function(err) {
                    return ValidationService.jsonResolveError(err, User, res);
                });

            }).catch(function(err) {
                return ValidationService.jsonResolveError(err, User, res);
            });
        }
    }
};
