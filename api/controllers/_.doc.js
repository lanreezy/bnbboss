/**
 * @apiDefine NotFoundExample
 * HTTP/1.1 404 Not Found
 * {
 *    "response": {
 *       "message": "{ModelName} not found"
 *    }
 * }
 */

/**
 * @apiDefine ValidationErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *    "response": {
 *       "message": "Validation error has occured",
 *       "errors": {
 *          "{fieldName}": [
 *             {
 *                "rule": "{validation rule failed}",
 *                "message": "{validation message}"
 *             }
 *          ]
 *       }
 *    }
 * }
 */