/**
 * Client Controller
 *
 * @description :: Server-side logic for managing clients
 */
var promise = require('bluebird');

module.exports = {

	/**
     * @apiDefine ClientData
     * @apiSuccess {Object} response variable                holding response
     * @apiSuccess {String} response.message                 response message
     * @apiSuccess {Object} response.data                    variable holding response data
     * @apiSuccess {String} response.data.firstName          first name of user
     * @apiSuccess {String} response.data.lastLast           last name of user
     * @apiSuccess {String} response.data.email              email address of user
     * @apiSuccess {String} response.data.role               user role (user or admin)
     * @apiSuccess {String} response.data.conciergeName      concierge name
     * @apiSuccess {integer} response.data.numListing         number of listings to add
     */
    
    /**
     * @apiDefine ClientDataMulti
     * @apiSuccess {Object} response variable                holding response
     * @apiSuccess {String} response.message                 response message
     * @apiSuccess {Object[]} response.data                  variable holding response data
     * @apiSuccess {String} response.data.firstName          first name of user
     * @apiSuccess {String} response.data.lastLast           last name of user
     * @apiSuccess {String} response.data.username           username of client
     * @apiSuccess {String} response.data.airbnbEmail		 airbnb email address of client
     * @apiSuccess {String} response.data.airbnbPassword 	 airbnb password of client
     * @apiSuccess {String} response.data.user 				 concierge user
     */
    
    /**
     * @api {post} /clients Create user
     * @apiName Create Client
     * @apiGroup Client
     * @apiVersion 0.0.1
     *
     * @apiParam {String} firstName                         user first name
     * @apiParam {String} lastName                          user last name
     * @apiParam {String} username           				username of client
     * @apiParam {String} airbnbEmail		 				airbnb email address of client
     * @apiParam {String} airbnbPassword 	 				airbnb password of client
     * @apiParam {String} user 				 				concierge user
     *
     * @apiUse ClientData
     *
     * @apiSuccessExample Success-Response
     * HTTP/1.1 200 OK
     * {
     *    "response": {
     *       "message": "Client created successfully.",
     *       "data": {
     *          "firstName": "Mark",
     *          "lastName": "Anthony",
     *          "username": "mark",
     *          "airbnbEmail": "kennedyidialu@gmail.com",
     *          "airbnbPassword": "marvel",
     *          "user": "56cc157f56d5f3602726d1d3",
     *          "createdAt": "2016-02-23T10:01:46.825Z",
     *          "updatedAt": "2016-02-23T10:01:46.825Z",
     *          "id": "56cc2e0a613185e429c3cd60"
     *       }
     *    }
     * }
     *
     * @apiUse ValidationErrorExample
     */
	create: function(req, res) {

        var data = req.body;
        data.username = data.username || ""
        data.user =  req.user.id

        // username must not have been reserved 
        var reservedNames = sails.config.settings.reservedNames;
        if (_.indexOf(reservedNames, data.username) !==  -1) {
            return ValidationService.uniquenessError("username", Client, res);
        } 

        // check if client with matching username previously existed
        Client.getByUsername(data.username).then(function(client){

            if (client) {
                return ValidationService.uniquenessError("username", Client, res);
            }

            // create new user
            Client.create(data).then(function createCB(createdClient) {
                return ResponseService.json(200, res, 'Client created successfully.', createdClient);
            }).catch(function(err) {
                return ValidationService.jsonResolveError(err, Client, res);
            });

        }).catch(function(err) {
            return ValidationService.jsonResolveError(err, Client, res);
        });
    },

    /**
     * @api {get} /clients/:id Fetch client by id
     * @apiName Fetch Client
     * @apiGroup Client
     * @apiVersion 0.0.1
     *
     * @apiParam {String} id      client's id
     *
     * @apiUse ClientData
     *
     * @apiSuccessExample Success-Response
     * HTTP/1.1 200 OK
     * {
     *    "response": {
     *       "message": "Client created successfully.",
     *       "data": {
     *          "firstName": "Mark",
     *          "lastName": "Anthony",
     *          "username": "mark",
     *          "airbnbEmail": "kennedyidialu@gmail.com",
     *          "airbnbPassword": "marvel",
     *          "isDeleted": false,
     *          "createdAt": "2016-02-23T10:45:23.570Z",
     *          "updatedAt": "2016-02-23T10:45:23.570Z",
     *          "id": "56cc3843a91e7afd2bd0f4a8",
     *          "user": {
     *          	"firstName": "John",
     *          	"lastName": "Doe",
     *          	"email": "john2@gmail.com",
     *          	"password": "$2a$10$kvO7RraGDyUed4ch0/yhc.nabH3845P6NrURmG0V.VcOwKtVHVVVu",
     *          	"role": "user",
     *          	"isDeleted": false,
     *          	"createdAt": "2016-02-23T08:17:03.171Z",
     *          	"updatedAt": "2016-02-23T08:17:03.171Z",
     *          	"id": "56cc157f56d5f3602726d1d3"
     *          }
     *       }
     *    }
     * }
     *
     * @apiUse NotFoundExample
     */
    read: function(req, res) {
        var id = req.params.id;
        Client.findOne({ id: id, isDeleted: false }).populateAll({ isDeleted: false }).exec(function(err, client) {
        	if (err) return ValidationService.jsonResolveError(err, Client, res);
            if (!client)  return ResponseService.json(404, res, 'Client not found');
            return ResponseService.json(200, res, 'Client retrieved successfully', client);
        })
    },

    /**
     * @api {get} /clients Fetch all clients
     * @apiName Fetch All Clients
     * @apiGroup Client
     * @apiVersion 0.0.1
     *
     * @apiUse ClientData
     *
     * @apiSuccessExample Success-Response
     * HTTP/1.1 200 OK
     * {
     *    "response": {
     *       "message": "Clients retrieved successfully",
     *       "data": {
     *          [
     *             {
     *                "firstName": "Mark",
     *                "lastName": "Anthony",
     *                "username": "mark",
     *                "airbnbEmail": "kennedyidialu@gmail.com",
     *                "airbnbPassword": "marvel",
     *                "isDeleted": false,
     *                "createdAt": "2016-02-23T10:45:23.570Z",
     *                "updatedAt": "2016-02-23T10:45:23.570Z",
     *                "id": "56cc3843a91e7afd2bd0f4a8",
     *                "user": {
     *                   "firstName": "John",
     *                   "lastName": "Doe",
     *                   "email": "john2@gmail.com",
     *                   "password": "$2a$10$kvO7RraGDyUed4ch0/yhc.nabH3845P6NrURmG0V.VcOwKtVHVVVu",
     *                   "role": "user",
     *                   "isDeleted": false,
     *                   "createdAt": "2016-02-23T08:17:03.171Z",
     *                   "updatedAt": "2016-02-23T08:17:03.171Z",
     *                   "id": "56cc157f56d5f3602726d1d3"
     *                 }
     *               }
     *           ]
     *       }
     *    }
     * }
     *
     * @apiUse ClientDataMulti
     */
    list: function(req, res) {
        Client.count().then(function(counter) {
            var perPage = req.query.perPage || 50;
            var skip = req.query.skip || 0;
            var foundClients = Client.find({
                isDeleted: false
            }).populateAll({
                isDeleted: false
            }).limit(perPage).skip(skip);
            return [promise.resolve(foundClients), counter, perPage, skip];
        }).spread(function(foundClients, counter, perPage, skip) {
            var meta = {
                total: counter,
                skipped: skip,
                perPage: perPage
            };
            return ResponseService.json(200, res, 'Clients retrieved successfully', foundClients, meta);
        }).catch(function(err) {
            return ValidationService.jsonResolveError(err, Client, res);
        });
    },


    /**
     * @api {put} /clients/:id Update client
     * @apiName Update Client
     * @apiGroup Client
     * @apiVersion 0.0.1
     *
     * @apiParam {String} firstName                         user first name
     * @apiParam {String} lastName                          user last name
     * @apiParam {String} username           				username of client
     * @apiParam {String} airbnbEmail		 				airbnb email address of client
     * @apiParam {String} airbnbPassword 	 				airbnb password of client
     * @apiParam {String} user 				 				concierge user
     *
     * @apiUse ClientData
     *
     * @apiSuccessExample Success-Response
     * HTTP/1.1 200 OK
     * {
     *    "response": {
     *       "message": "User updated successfully",
     *       "data": {
     *          "data": {
     *          "firstName": "Mark",
     *          "lastName": "Anthony",
     *          "username": "mark",
     *          "airbnbEmail": "kennedyidialu@gmail.com",
     *          "airbnbPassword": "marvel",
     *          "user": "56cc157f56d5f3602726d1d3",
     *          "createdAt": "2016-02-23T10:01:46.825Z",
     *          "updatedAt": "2016-02-23T10:01:46.825Z",
     *          "id": "56cc2e0a613185e429c3cd60"
     *       }
     *    }
     * }
     *
     * @apiUse ValidationErrorExample
     * @apiUse NotFoundExample
     */
    update: function(req, res) {
        var data = req.body;

        // concierge user shouldn't be able to change associated user
        if (req.user.role == "user") {
        	delete data.user
    	}

        Client.update({ id: req.param('id') }, data).then(function(updatedClient) {
            if (!updatedClient.length) {
                return ResponseService.json(404, res, "Client not found");
            }
            return ResponseService.json(200, res, "Updated client successfully", updatedClient[0]);
        }).catch(function(err) {
            return ValidationService.jsonResolveError(err, Client, res);
        });
    },

    /**
     * @api {delete} /clients/:id Remove client
     * @apiName Remove Client
     * @apiGroup Client
     * @apiVersion 0.0.1
     *
     * @apiParam {String} id      client's id
     *
     * @apiUse ClientData
     *
     * @apiSuccessExample Success-Response
     * HTTP/1.1 200 OK
     * {
     *    "response": {
     *       "message": "Client removed successfully",
     *    }
     * }
     *
     * @apiUse NotFoundExample
     */
    delete: function(req, res) {
        Client.findOne({ id: req.param('id'), isDeleted: false }).then(function(existingClient){
            if (!existingClient) {
                return ResponseService.json(404, res, "Client not found");
            } 
            existingClient.isDeleted = true;
            existingClient.save(function(err, updatedClient){
                if (err) ValidationService.jsonResolveError(err, Client, res);
                return ResponseService.json(200, res, "Client removed successfully");
            });
        }).catch(function(err) {
            return ValidationService.jsonResolveError(err, Client, res);
        });
    }
}