var passport    = require("passport"),
    rp          = require('request-promise'),
    bcrypt      = require("bcrypt");

module.exports = {

    /**
    * @api {post} /auth/login Login User
    * @apiVersion 0.1.0
    * @apiName User Login
    * @apiGroup Auth
    *
    * @apiParam {string} email Email
    * @apiParam {string} password Password
    *
    * @apiSuccessExample Success-Response:
    * {
    *    "response": {
    *       "message": "Login successful",
    *       "data": {
    *          "user": {
    *             "firstName": "John",
    *             "lastName": "Doe",
    *             "email": "john2@gmail.com",
    *             "role": "user",
    *             "isDeleted": false,
    *             "createdAt": "2016-02-23T08:17:03.171Z",
    *             "updatedAt": "2016-02-23T08:17:03.171Z",
    *             "id": "56cc157f56d5f3602726d1d3"
    *          },
    *          "token": "eyJOjQ1NzUyODc5MjF9....1TEN0IV_QCLnEFVc"
    *       }
    *    }
    * }
    *
    * @apiErrorExample Error-Response:
    * HTTP/1.1 401 Unauthorized
    * {
    *    "response": {
    *       "message": "Invalid login credentials"
    *    }
    * }
    */    
    login: function (req, res) {

        var data = req.body;

        if (!data.email || !data.password) {
            return ResponseService.json(401, res, 'Invalid login credentials');
        }

        User.findOne({ email: data.email, isDeleted: false }).populateAll({ isDeleted: false }).then(function(user){
            
            if (!user) {
                return ResponseService.json(401, res, 'Invalid login credentials');   
            }

            // verify password
            if (!bcrypt.compareSync(data.password, user.password)) {
                return ResponseService.json(401, res, 'Invalid login credentials'); 
            }

            var token = JwtService.issueToken(user);
            return ResponseService.json(200, res, 'Login successful', {user: user, token: token});

        }).catch(function(err){
            return ValidationService.jsonResolveError(err, User, res);
        });
    }
};
