/**
 * Notification Controller
 */

var Promise = require('bluebird');

module.exports = {

	/**
     * @apiDefine NotificationData
     * @apiSuccess {Object} response variable                holding response
     * @apiSuccess {String} response.message                 response message
     * @apiSuccess {Object} response.data                    variable holding response data
     * @apiSuccess {Object} response.data.client 			 client for whom notification is for
     * @apiSuccess {String} response.data.type 			 	 type of notification
     * @apiSuccess {String} response.data.from 				 email address of sender
     * @apiSuccess {String} response.data.subject 			 subject of notification
     * @apiSuccess {String} response.data.message 			 html version of message 
     * @apiSuccess {String} response.data.messageText		 text version of message 
     * @apiSuccess {Boolean} response.data.read 		     flag to tell if message has been read or not
     */
    
    /**
     * @apiDefine NotificationDataMulti
     * @apiSuccess {Object} response variable                holding response
     * @apiSuccess {String} response.message                 response message
     * @apiSuccess {Object[]} response.data                  variable holding response data
     * @apiSuccess {Object} response.data.client 			 client for whom notification is for
     * @apiSuccess {String} response.data.type 			 	 type of notification
     * @apiSuccess {String} response.data.from 				 email address of sender
     * @apiSuccess {String} response.data.subject 			 subject of notification
     * @apiSuccess {String} response.data.message 			 html version of message 
     * @apiSuccess {String} response.data.messageText		 text version of message 
     * @apiSuccess {Boolean} response.data.read 		     flag to tell if message has been read or not
     */
    
    /**
     * @api {post} /notifications Create a notification
     * @apiName Create Notification
     * @apiGroup Notification
     * @apiVersion 0.0.1
     *
     * @apiParam {Object} client 			 	client for whom notification is for
     * @apiParam {String} type 			 	 	type of notification
     * @apiParam {String} from 				 	email address of sender
     * @apiParam {String} subject 			 	subject of notification
     * @apiParam {String} message 			 	html version of message 
     * @apiParam {String} messageText		 	optional. text version of message 
     * @apiParam {Boolean} read 				flag to tell if message has been read or not
     *
     * @apiUse NotificationData
     *
     * @apiSuccessExample Success-Response
     * HTTP/1.1 200 OK
     * {
     * 		"response": {
     * 			"message": "Notification created successfully.",
     * 	 		"data": {
     * 	  			"client": "xxxxxxxxx",
     * 	    		"type": "booking",
     * 	     		"from": "ken@gmail.com",
     * 	      		"subject": "How are you",
     * 		        "message": "Can you stop?",
     * 	     	    "read": false,
     * 	      	    "isDeleted": false,
     * 	       	    "createdAt": "2016-02-29T13:23:19.340Z",
     * 	            "updatedAt": "2016-02-29T13:23:19.341Z",
     * 	            "id": "56d4464772eebc464b4bfc04"
     * 	         }
     * 	      }
     * 	  }
     * }
     *
     * @apiUse ValidationErrorExample
     */
	create: function(req, res) {
		var data = req.body;
		Notification.create(data).then(function createCB(createdNotification) {
            return ResponseService.json(200, res, 'Notification created successfully.', createdNotification);
        }).catch(function(err) {
            return ValidationService.jsonResolveError(err, Notification, res);
        });
	},

	/**
     * @api {get} /notifications/:id Fetch notification by id
     * @apiName Fetch Notification
     * @apiGroup Notification
     * @apiVersion 0.0.1
     *
     * @apiParam {String} id      notification id
     *
     * @apiUse NotificationData
     * 
     * @apiSuccessExample Success-Response
     * HTTP/1.1 200 OK
     * {
     * 		"response": {
     * 			"message": "Notification retrieved successfully.",
     * 	 		"data": {
     * 	  			"client": {
     * 	  			   	"firstName": "Mark",
     *          	  	"lastName": "Anthony",
     *              	"username": "mark",
     *               	"airbnbEmail": "kennedyidialu@gmail.com",
     *                	"airbnbPassword": "marvel",
     *                 	"user": "56cc157f56d5f3602726d1d3",
     *                  "createdAt": "2016-02-23T10:01:46.825Z",
     *                  "updatedAt": "2016-02-23T10:01:46.825Z",
     *                  "id": "56cc2e0a613185e429c3cd60"
     * 	  			},
     * 	    		"type": "booking",
     * 	     		"from": "ken@gmail.com",
     * 	      		"subject": "How are you",
     * 		        "message": "Can you stop?",
     * 	     	    "read": false,
     * 	      	    "isDeleted": false,
     * 	       	    "createdAt": "2016-02-29T13:23:19.340Z",
     * 	            "updatedAt": "2016-02-29T13:23:19.341Z",
     * 	            "id": "56d4464772eebc464b4bfc04"
     * 	         }
     * 	      }
     * 	  }
     * }
     * 
     * @apiUse NotFoundExample
     */
	read: function (req, res) {
		var id = req.params.id;
        Notification.findOne({ id: id, isDeleted: false }).populateAll({ isDeleted: false }).exec(function(err, notification) {
        	if (err) return ValidationService.jsonResolveError(err, Notification, res);
            if (!notification)  return ResponseService.json(404, res, 'Notification not found');
            return ResponseService.json(200, res, 'Notification retrieved successfully', notification);
        })
	},


	/**
     * @api {get} /notifications Fetch all notifications
     * @apiName Fetch All Notifications
     * @apiGroup Notification
     * @apiVersion 0.0.1
     *
     * @apiUse NotificationData
     * @apiParam {String} client_id    optional. client id to filter by 
     * @apiParam {Boolean} read 	   optional. filter by read status (true or false)
     * @apiParam {Number} perPage  	   optional. Number of page to return 
     * @apiParam {Number} skip 		   optional. Number of pages to skip
     *
     * @apiSuccessExample Success-Response
     * HTTP/1.1 200 OK
     * {
     *    "response": {
     *       "message": "Notifications retrieved successfully",
     *       "data": {
     *          [
     *             {
     * 	  			"client": {
     * 	  			   	"firstName": "Mark",
     *          	  	"lastName": "Anthony",
     *              	"username": "mark",
     *               	"airbnbEmail": "kennedyidialu@gmail.com",
     *                	"airbnbPassword": "marvel",
     *                 	"user": "56cc157f56d5f3602726d1d3",
     *                  "createdAt": "2016-02-23T10:01:46.825Z",
     *                  "updatedAt": "2016-02-23T10:01:46.825Z",
     *                  "id": "56cc2e0a613185e429c3cd60"
     * 	  			},
     * 	    		"type": "booking",
     * 	     		"from": "ken@gmail.com",
     * 	      		"subject": "How are you",
     * 		        "message": "Can you stop?",
     * 	     	    "read": false,
     * 	      	    "isDeleted": false,
     * 	       	    "createdAt": "2016-02-29T13:23:19.340Z",
     * 	            "updatedAt": "2016-02-29T13:23:19.341Z",
     * 	            "id": "56d4464772eebc464b4bfc04"
     * 	         }
     *           ]
     *       }
     *    }
     * }
     *
     * @apiUse NotificationDataMulti
     */
	list: function (req, res) {

		var where = { isDeleted: false };

		if (req.query.client_id)
			where.client = req.query.client_id

		if (req.query.read) 
			where.read = (req.query.read == "false") ? false : true;

		Notification.count(where).then(function(counter) {
            var perPage = req.query.perPage || 50;
            var skip = req.query.skip || 0;
            var foundNotifications = Notification.find(where).limit(perPage).skip(skip);
            return [Promise.resolve(foundNotifications), counter, perPage, skip];
        }).spread(function(foundNotifications, counter, perPage, skip) {
            var meta = {
                total: counter,
                skipped: skip,
                perPage: perPage
            };
            return ResponseService.json(200, res, 'Notifications retrieved successfully', foundNotifications, meta);
        }).catch(function(err) {
            return ValidationService.jsonResolveError(err, Notification, res);
        });
	},


	/**
     * @api {put} /notifications/:id Update a notification
     * @apiName Update Notification
     * @apiGroup Notification
     * @apiVersion 0.0.1
     *
     * @apiParam {Object} client 			 	client for whom notification is for
     * @apiParam {String} type 			 	 	type of notification
     * @apiParam {String} from 				 	email address of sender
     * @apiParam {String} subject 			 	subject of notification
     * @apiParam {String} message 			 	html version of message 
     * @apiParam {String} messageText		 	optional. text version of message 
     * @apiParam {Boolean} read 				flag to tell if message has been read or not
     *
     * @apiUse NotificationData
     *
     * @apiSuccessExample Success-Response
     * {
     * 		"response": {
     * 			"message": "Notification updated successfully.",
     * 	 		"data": {
     * 	  			"client": {
     * 	  			   	"firstName": "Mark",
     *          	  	"lastName": "Anthony",
     *              	"username": "mark",
     *               	"airbnbEmail": "kennedyidialu@gmail.com",
     *                	"airbnbPassword": "marvel",
     *                 	"user": "56cc157f56d5f3602726d1d3",
     *                  "createdAt": "2016-02-23T10:01:46.825Z",
     *                  "updatedAt": "2016-02-23T10:01:46.825Z",
     *                  "id": "56cc2e0a613185e429c3cd60"
     * 	  			},
     * 	    		"type": "booking",
     * 	     		"from": "ken@gmail.com",
     * 	      		"subject": "How are you",
     * 		        "message": "Can you stop?",
     * 	     	    "read": false,
     * 	      	    "isDeleted": false,
     * 	       	    "createdAt": "2016-02-29T13:23:19.340Z",
     * 	            "updatedAt": "2016-02-29T13:23:19.341Z",
     * 	            "id": "56d4464772eebc464b4bfc04"
     * 	         }
     * 	      }
     * 	  }
     * }
     *
     * @apiUse ValidationErrorExample
     * @apiUse NotFoundExample
     */
	update: function(req, res) {
        var data = req.body;
        Notification.update({ id: req.param('id') }, data).then(function(updatedNotification) {
            if (!updatedNotification.length) {
                return ResponseService.json(404, res, "Notification not found");
            }
            return ResponseService.json(200, res, "Updated notification successfully", updatedNotification[0]);
        }).catch(function(err) {
            return ValidationService.jsonResolveError(err, Notification, res);
        });
    },


    /**
     * @api {delete} /notifications/:id Remove notification
     * @apiName Remove Notification
     * @apiGroup Notification
     * @apiVersion 0.0.1
     *
     * @apiParam {String} id      client's id
     *
     * @apiUse NotificationData
     *
     * @apiSuccessExample Success-Response
     * HTTP/1.1 200 OK
     * {
     *    "response": {
     *       "message": "Notification removed successfully",
     *    }
     * }
     *
     * @apiUse NotFoundExample
     */
	delete: function (req, res) {
		Notification.findOne({ id: req.param('id'), isDeleted: false }).then(function(existingNotification){
            if (!existingNotification) {
                return ResponseService.json(404, res, "Notification not found");
            } 
            existingNotification.isDeleted = true;
            existingNotification.save(function(err, updatedClient){
                if (err) ValidationService.jsonResolveError(err, Notification, res);
                return ResponseService.json(200, res, "Notification removed successfully");
            });
        }).catch(function(err) {
            return ValidationService.jsonResolveError(err, Notification, res);
        });
	}
}