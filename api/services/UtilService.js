/**
 * Utility functions
 */

module.exports = {

	/**
	 * Get name from email address
	 * @param  {string} email email address
	 * @return {string}       username part of email
	 */
	getEmailName: function (email) {
		return email.substring(0, email.indexOf("@"));
	},

	/**
	 * The host name of the email address
	 * @param  {string} email email address
	 * @return {string}       host name
	 */
	getEmailHost: function (email) {
		return email.substring(email.indexOf("@") + 1, email.length)
	}
}