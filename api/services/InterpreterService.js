/**
 * Interpreter services contains various methods to
 * detect message content type and extract specific data from message
 */

module.exports = {

	// added any interpreter function here.
	funcs: {

		/**
		 * Checks if a message is an inquiry message
		 * @param  {object}  msg message object
		 * @return {object}      notification object or false
		 */
		isInquiry: function(msg) {
			var titleMatchRegEx = new RegExp(/^(RE:\s)?Inquiry at.*?$/ig)
			if (msg.subject && titleMatchRegEx.test(msg.subject)) {
				return {
					type: "inquiry",
					from: msg.from.address,
					subject: msg.subject,
					message: msg.html,
					messageText: msg.text
				}
			}
			return false
		},

		/**
		 * Checks if a message is an booking message
		 * @param  {object}  msg message object
		 * @return {object}      notification object or false
		 */
		isBooking: function(msg) {
			var titleMatchRegEx = new RegExp(/^Reservation Request \-.*?$/ig)
			if (msg.subject && titleMatchRegEx.test(msg.subject)) {
				return {
					type: "booking",
					from: msg.from.address,
					subject: msg.subject,
					message: msg.html,
					messageText: msg.text
				}
			}
			return false
		},

		/**
		 * Checks if a message is an booking reply message
		 * @param  {object}  msg message object
		 * @return {object}      notification object or false
		 */
		isBookingReply: function(msg) {
			var titleMatchRegEx = new RegExp(/^(RE:\s)?Reservation request at.*?$/ig)
			if (msg.subject && titleMatchRegEx.test(msg.subject)) {
				return {
					type: "booking_reply",
					from: msg.from.address,
					subject: msg.subject,
					message: msg.html,
					messageText: msg.text
				}
			}
			return false
		},

		/**
		 * Checks if a message is a pending reservation request
		 * @param  {object}  msg message object
		 * @return {object}      notification object or false
		 */
		isPending: function(msg) {
			var titleMatchRegEx = new RegExp(/^Pending: Reservation Request.*?$/ig)
			if (msg.subject && titleMatchRegEx.test(msg.subject)) {
				return {
					type: "pending",
					from: msg.from.address,
					subject: msg.subject,
					message: msg.html,
					messageText: msg.text
				}
			}
			return false
		},

		/**
		 * Checks if a message is a confirmed reservation
		 * @param  {object}  msg message object
		 * @return {object}      notification object or false
		 */
		isReservationConfirmed: function(msg) {
			var titleMatchRegEx = new RegExp(/^Reservation Confirmed.*?$/ig)
			if (msg.subject && titleMatchRegEx.test(msg.subject)) {
				return {
					type: "reservation_confirmed",
					from: msg.from.address,
					subject: msg.subject,
					message: msg.html,
					messageText: msg.text
				}
			}
			return false
		},

		/**
		 * Checks if a message is a payment received message
		 * @param  {object}  msg message object
		 * @return {object}      notification object or false
		 */
		isPaymentReceived: function(msg) {
			return false
		},

		/**
		 * Checks if a message is an inquiry message
		 * @param  {object}  msg message object
		 * @return {object}      notification object or false
		 */
		isCancellation: function(msg) {
			var titleMatchRegEx = new RegExp(/^Reservation.*Canceled$/ig)
			if (msg.subject && titleMatchRegEx.test(msg.subject)) {
				return {
					type: "canceled",
					from: msg.from.address,
					subject: msg.subject,
					message: msg.html,
					messageText: msg.text
				}
			}
			return false
		}

	}
}