var ejs = require('ejs'),
    fs = require('fs'),
    Promise = require("bluebird"),
    SparkPost   = require('sparkpost'),
    spClient    = new SparkPost(sails.config.settings.sparkPostKey);

module.exports = {
    send: function(msg, msgHTML, subject, fromEmail, toEmail, replyTo) {
        return new Promise(function(resolve, reject){
            var reqOpts = {
                transmissionBody: {
                    recipients: [
                        {
                            address: {
                                email: toEmail.address,
                                name: toEmail.name
                            }
                        }
                    ],
                    content: {
                        from: {
                            name: fromEmail.name,
                            email: fromEmail.address
                        },
                        subject: subject,
                        reply_to: replyTo,
                        text: msg,
                        html: msgHTML
                    }
                }
            };
            
            spClient.transmissions.send(reqOpts, function(err, res) {
                if (err) {
                    return reject(err);
                }
                return resolve(res.body);
            });
        });
    }
};
