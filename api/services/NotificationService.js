/**
 * Handle Notification Related Functionalities
 */

var Promise = require("bluebird");

module.exports = {

	/**
	 * Checks if a specific host name belongs to the allowed list of host name to receive message from
	 * @param  {string}  host host name to check
	 * @return {Boolean}      true or false
	 */
	isKnownHost: function (host) {
		var hosts = ["airbnb.com", "airbnbmail.com", "e.airbnb.com", "host.airbnb.com", "guest.airbnb.com", "airbnb.zendesk.com", "express.medallia.com", "reply.airbnb.com"];
		hosts.push(sails.config.settings.emailDomain)
		var match = false
		_.each(host, function(v){
			if (v === host.toLowerCase()) {
				match = true;
				return false;
			}
		})
		return match
	},

	/**
	 * Forwards message received from airbnb to client's personal email
	 * @param  {object} msg         the message object
	 * @param  {string} client the 	client object
	 */
	forwardToClient: function (msg, client, replyTo) {
		var to = { address: client.airbnbEmail, name: msg.to.name }
		var from = { address: client.username + '@' + sails.config.settings.emailDomain, name: msg.from.name }
		return EmailService.send(msg.text, msg.html, msg.subject, from, to, replyTo).then(function(resp){
			console.log("Successfully forwarded message: ", resp)
		}).catch(function(err){
			console.error("Error forwarding message: ", err)
		})
	},

	/**
	 * Handle message from SMTP server
	 * @param  {object} msg message meta
	 */
	handleMessages: function (msg) {
		
		// incoming message failed dkim test
		if (!msg.dkim) {
			return console.log("NotificationService: Failed DKIM: ", msg.dkim)		
		}
		
		// get the username of the receipient
		var username = UtilService.getEmailName(msg.to.address)
		var toHost = UtilService.getEmailHost(msg.to.address)

		// get the host name of sender 
		var fromHost = UtilService.getEmailHost(msg.from.address)

		// reject unwanted host emails
		if (process.env.NODE_ENV == "production" && !this.isKnownHost(fromHost)) {
			console.log("Notification: New message is not from recognized hosts. It has host = ", fromHost)
			return;
		}

		// if email is sent to any of the reserved usernames, send to admin mailbox
		if (_.indexOf(sails.config.settings.reservedNames, username) != -1) {
			Mail.create({
				username: username,
			    type: "incoming",
			    from: msg.from.address,
			    subject: msg.subject,
			    message: msg.html,
			    messageText: msg.text
			}).then(function(mail){
				console.log("Mail received from ", msg.from.address)
			})
			return 
		}

		// get reply to address
		var replyTo = (msg.replyTo) ? msg.replyTo.address : null;

		// find client by username
		return Client.getByUsername(username).then(function(client){

			// client with username does not exist. Log and do nothing
			if (!client) {
				return console.log("NotificationService: Client with username ("+username+") does not exist ")
			}

			// forward messsage to client
			NotificationService.forwardToClient(msg, client, replyTo)

			// interpret message
			NotificationService.interpret(client, msg, function(notificationObj){

				notificationObj.replyTo = replyTo;

				// Add notificaion
				Notification.create(notificationObj).then(function(newNotification){
					console.log("Notification Added")
				}).catch(function(err){
					console.log("NotificationService: Error adding notification: ", err.message)
				});

			})

		}).catch(function(err){
			console.log("NotificationService: Error finding client: ", err.message)
		})
	},

	/**
	 * Interpret a message into a notification
	 * @param  {object} client bnbboss client
	 * @param  {object} msg    smtp server message object
	 * @return {[type]}        [description]
	 */
	interpret: function (client, msg, cb) {

		// holds the create notification 
		var notificationObj = null;

		// get all interpreter functions and attempt to use each to interpret the message
		var interpreterFuncs = Object.keys(InterpreterService.funcs)
		interpreterFuncs.forEach(function(fname){
			if (!notificationObj) {
				notificationObj = InterpreterService.funcs[fname](msg)
				if (notificationObj) {
					notificationObj.client = client.id
				}
			}
		});

		// message could not be interpreted.
		// Create a custom notification for unknown messages
		if (!notificationObj) {
			var notificationObj = {
				client: client.id,
				type: "custom",
				from: msg.from.address,
				subject: msg.subject,
				message: msg.html,
				messageText: msg.text
			}
		}

		cb(notificationObj)
	}
}