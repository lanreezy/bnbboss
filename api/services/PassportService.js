var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var BasicStrategy = require('passport-http').BasicStrategy;
var apiKey = sails.config.settings.apiKey;
var jwtSecret = sails.config.settings.jwtSecret;

// BasicAuth authentication between app and API server.
// format: Authorization: Base64(apiKey:jwtSecret)
passport.use(new BasicStrategy( function(_apiKey, _jwtSecret, done) {

        // invalid api key
        if (_apiKey != apiKey) 
            return done(null, false, {message: "Invalid app credentials"});
        
        // invalid jwt secret
        if (_jwtSecret != jwtSecret)
            return done(null, false, {message: "Invalid app credentials"});

        return done(null, true);
    }
));

// passport.use(new LocalStrategy({usernameField: 'email', passwordField: 'password', passReqToCallback: true}, function(req, username, password, done) {
//     authenticate(req, username, password, done);
// }));
