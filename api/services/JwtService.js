var jwt = require('jsonwebtoken');
var jwtSecret = sails.config.settings.jwtSecret;
var tokenExpiryInSeconds = sails.config.settings.tokenExpiryInSeconds;

module.exports = {
    issueToken: function (payload, expirytime) {
        var expiry = (expirytime) ? expirytime : tokenExpiryInSeconds;
        var token = jwt.sign(payload, process.env.TOKEN_SECRET || jwtSecret, {
            expiresIn: expiry / 60
        });
        return token;
    },
    verifyToken: function (token, cb) {
        return jwt.verify(token, process.env.TOKEN_SECRET || jwtSecret, {}, cb);
    }
};
