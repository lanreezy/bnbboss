/**
 * SMTP Service
 */

var mailin = require('mailin');

module.exports = {

	/**
	 * Start the SMTP Server
	 * @param  {[type]} options [description]
	 * @return {[type]}         [description]
	 */
	startServer: function (options) {

		mailin.start({
			port: sails.config.settings.smtpServerPort,
		  	disableWebhook: true 
		});

		mailin.on('startMessage', function (connection) {
			console.log("New connection: ", connection)
			// perfom rejection logic here
		})

		mailin.on('error', function (err) {
			console.log("Err: ", err)
		})

		/* Event emitted after a message was received and parsed. */
		mailin.on('message', function (connection, data, content) {
			
		  	var msg = {}
		  	msg.dkim = (data.dkim === 'pass') ? true : false;

		  	if (data.from.length) {
		  		msg.from = data.from[0]
		  	}

		  	if (data.to.length > 0) {
		  		msg.to = data.to[0]
		  	}

		  	msg.subject = data.subject
		  	msg.html = data.html
		  	msg.text = data.text

		  	if (data.replyTo && data.replyTo.length > 0) {
		  		msg.replyTo = data.replyTo[0]
		  	}

		  	NotificationService.handleMessages(msg, connection)
		});
	}
}