/**
 * Notification.js
 *
 * @description :: Notification model
 */

var Promise = require("bluebird");

module.exports = {
    attributes: {
        client: {
        	model: 'client',
            required: true
        },
        type: {	
        	type: 'string',
            required: true
        },
        data: {
            type: 'object'
        },
        read: {
            type: 'boolean',
            defaultsTo: false
        },
        from: {
            type: 'string',
            required: true,
            email: true
        },
        subject: {
            required: true,
        	type: 'string'
        },
        message: {
        	type: 'string',
            required: true
        },
        messageText: {
            type: 'string'
        },
        replyTo: {
            type: 'string',
            email: true
        },
        isDeleted: {
            type: 'boolean',
            defaultsTo: false
        }
    },

    validationMessages: {
        client: {
            required: "client id is required"
        },
        type: {
            required: "type is required"
        },
        from: {
            required: "from address is required",
            email: "email is invalid"
        },
        subject: {
            required: "subject is required"
        },
        message: {
            required: "message is required"
        },
        replyTo: {
            email: "email is invalid"
        }
    }
}