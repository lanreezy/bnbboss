/**
 * User.js
 *
 * @description :: User model
 */

var Promise = require("bluebird"),
    bcrypt  = require("bcrypt");

module.exports = {
    attributes: {
        firstName: {
            type: 'string',
            required: true
        },
        lastName: {
            type: 'string',
            required: true
        },
        email: {
            type: 'string',
            required: true,
            unique: true,
            email: true
        },
        password: {
            type: 'string',
            required: true,
            minLength: 6
        },
        role: {
            type: 'string',
            required: true
        },
        conciergeName: {
            type: 'string',
        },
        numListing: {
            type: 'integer'
        },
        isDeleted: {
            type: 'boolean',
            defaultsTo: false
        },

        toJSON: function(){
            var obj = this.toObject();
            delete obj.password;
            return obj;
        }
    },

    beforeCreate: function (values, cb) {
        bcrypt.hash(values.password, 10, function(err, hash) {
            if(err) return cb(err);
            values.password = hash;
            cb();
        });
    },

    /**
     * Find a user by email
     * @param  {string}  email the email address
     * @return {Promise<object>}
     */
    getByEmail: function (email) {
        return new Promise(function(resolve, reject){
            User.findOneByEmail(email).then(function(user){
                return resolve(user);
            }).catch(reject);
        })
    },

    // validation messages
    validationMessages: {
        firstName: {
            required: "First name is required"
        },
        lastName: {
            required: "Last name is required"
        },
        email: {
            required: "Email is required",
            unique: "Email is already registered",
            email: "Email is invalid"
        },
        password: {
            required: "Password is required",
            minLength: "Password is too short. Minimum of 6 characters required"
        },
        role: {
            required: "Role is required"
        }
    }
};

