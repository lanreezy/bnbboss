/**
 * User.js
 *
 * @description :: User model
 */

var Promise = require("bluebird");

module.exports = {
    attributes: {
        user: {
        	model: 'user'
        },
        username: {					// bnbboss username 
        	type: 'string',
        	unique: true,
        	required: true
        },
        firstName: {
            type: 'string',
            required: true
        },
        lastName: {
            type: 'string',
            required: true
        },
        airbnbEmail: {
            type: 'string',
            required: true,
            email: true
        },
        airbnbUsername: {
        	type: 'string'
        },
        airbnbPassword: {
        	type: 'string',
            required: true
        },
        isDeleted: {
            type: 'boolean',
            defaultsTo: false
        }
    },

    /**
     * Find a user by username
     * @param  {string}  username the username
     * @return {Promise<object>}
     */
    getByUsername: function (username) {
        return new Promise(function(resolve, reject){
            Client.findOne({ username: username, isDeleted: false }).then(function(user){
                return resolve(user);
            }).catch(reject);
        });
    },

    validationMessages: {
        firstName: {
            required: "First name is required"
        },
        lastName: {
            required: "Last name is required"
        },
        username: {
        	required: "Username is required",
        	unique: "Username is already registered"
        },
        airbnbEmail: {
        	required: "Airbnb email is required",
        	email: "Airbnb email is invalid"
        },
        airbnbPassword: {
        	required: "Airbnb password is required"
        }
    }
}