/**
 * Mail model holds messages sent to any 
 * reserve email. Add reserve email username in config/settings.js
 */

var Promise = require("bluebird");

module.exports = {
    attributes: {
        username: {
        	type: 'string',
            required: true
        },
        read: {
            type: 'boolean',
            defaultsTo: false
        },
        from: {
            type: 'string',
            required: true,
            email: true
        },
        subject: {
            required: true,
        	type: 'string'
        },
        message: {
        	type: 'string',
            required: true
        },
        messageText: {
            type: 'string'
        },
        replyTo: {
            type: 'string',
            email: true
        },
        isDeleted: {
            type: 'boolean',
            defaultsTo: false
        }
    },

    validationMessages: {
        username: {
            required: "username is required"
        },
        from: {
            required: "from address is required",
            email: "email is invalid"
        },
        subject: {
            required: "subject is required"
        },
        message: {
            required: "message is required"
        }
    }
}