/**
 * Basic Authentication for client and server authentication
 */

var passport = require("passport");

module.exports = function(req, res, next) {
	passport.authenticate('basic', { session: false }, function (err, user, info) {
		if (err) 
			return ValidationService.jsonResolveError(err, {}, res);
		if (!user) {
    		return ResponseService.json(401, res, "Authentication failed. Invalid credentials");
		}
		return next()
  	})(req, res, next);
}