/**
 * Checks if logged in user is an admin
 */

module.exports = function(req, res, next) {
	if (req.user && req.user.role !== "admin") {
		return res.json(401, {
			response: {
				message: "User does not have permission to this perform operation"
			}
		});
	}
	next()
}