var sails = require('sails');
var Promise = require("bluebird");

before(function(done) {

    // Increase the Mocha timeout so that Sails has enough time to lift.
    this.timeout(5000);

    sails.lift({
        port: 1501,
        settings: {
            smtpServerPort: 26
        },
        models: {
            connection: 'testMongoDB',
            migrate: 'alter'
        }
    }, function(err, server) {
    if (err) return done(err);
        // here you can load fixtures, etc.
        done(err, sails);
    });
});

after(function(done) {
    Promise.all([User.destroy(), Client.destroy()]).then(function(){
        sails.lower(done);
    });
});