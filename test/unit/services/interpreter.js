var after = require('after'),
	should = require('should'),
	request = require('supertest'),
	randomstring = require('randomstring'),
	fs = require("fs"),
	MailParser = require("mailparser").MailParser,
	InterpreterService = require("../../../api/services/InterpreterService"),
	fx = require('node-fixtures');

describe('InterpreterService', function(){

	function parseMessage(fileName, cb) {
		var content = fs.readFileSync(__dirname + "/../../messages/" + fileName + '.txt').toString();
		var mp = new MailParser();
		mp.on("end", function(data){
			var msg = {};
			msg.from = data.from[0];
		  	msg.to = data.to[0];
		  	msg.subject = data.subject;
		  	msg.html = data.html;
		  	msg.text = data.text;
		  	return cb(msg)
		});
		mp.write(content);
		mp.end();
	}

	describe(".isInquiry()", function(){

		it("should successfully interpret inquiry message", function(done){
			parseMessage("inquiry", function(msg){
				var retval = InterpreterService.funcs.isInquiry(msg)
				retval.should.not.be.equal(false)
				done()
			});
		})

		it("should successfully interpret inquiry reply message", function(done){
			parseMessage("inquiry_reply", function(msg){
				var retval = InterpreterService.funcs.isInquiry(msg)
				retval.should.not.be.equal(false)
				done()
			});
		})

		it("should fail to interpret pending reservation request sent by airbnb", function(done){
			parseMessage("pending_request", function(msg){
				var retval = InterpreterService.funcs.isInquiry(msg)
				retval.should.be.equal(false)
				done()
			});
		});

	});

	describe(".isBookingReply()", function(){

		it("should successfully interpret booking request message reply", function(done){
			parseMessage("reservation_reply", function(msg){
				var retval = InterpreterService.funcs.isBookingReply(msg)
				retval.should.not.be.equal(false)
				done()
			});
		});

	});

	describe(".isBooking()", function(){

		it("should successfully interpret booking request message", function(done){
			parseMessage("booking_request", function(msg){
				var retval = InterpreterService.funcs.isBooking(msg)
				retval.should.not.be.equal(false)
				done()
			});
		});

	});

	describe(".isPending()", function(){

		it("should successfully interpret pending reservation request message", function(done){
			parseMessage("pending_request", function(msg){
				var retval = InterpreterService.funcs.isPending(msg)
				retval.should.not.be.equal(false)
				done()
			});
		});

	});

	describe(".isReservationConfirmed()", function(){

		it("should successfully interpret confirmed reservation request", function(done){
			parseMessage("reservation_confirmed", function(msg){
				var retval = InterpreterService.funcs.isReservationConfirmed(msg)
				retval.should.not.be.equal(false)
				done()
			});
		});

	});

	describe(".isCancellation()", function(){

		it("should successfully interpret cancelled reservation request", function(done){
			parseMessage("reservation_canceled", function(msg){
				var retval = InterpreterService.funcs.isCancellation(msg)
				retval.should.not.be.equal(false)
				done()
			});
		});

	});

});