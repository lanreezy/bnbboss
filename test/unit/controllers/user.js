// User controller tests
var after = require('after'),
	should = require('should'),
	request = require('supertest'),
	randomstring = require('randomstring'),
	fx = require('node-fixtures');

describe('UserController', function(){

	describe("create()", function (){

		var email = randomstring.generate(5) + "@gmail.com"

		it("should return multiple validation errors when empty request body is provided", function(done){
			request(sails.hooks.http.app)
				.post('/users')
				.send({})
				.expect(400)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.have.property("errors");
					res.body.response.errors.should.have.property("firstName")
					res.body.response.errors.should.have.property("lastName")
					res.body.response.errors.should.have.property("email")
					res.body.response.errors.should.have.property("password")
					res.body.response.errors.should.have.property("role")
					done()				
				});
		});

		it("should fail when email address is invalid", function(done){
			var user = _.cloneDeep(fx.users.user0)
			user.email = randomstring.generate(5) + "gmail.com"
			request(sails.hooks.http.app)
				.post('/users')
				.send(user)
				.expect(400)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.have.property("errors");
					res.body.response.errors.should.have.property("email")
					res.body.response.errors.email.should.containEql({ rule: 'email', message: 'Email is invalid' })
					done()				
				});
		});

		it("should successfully create a user", function(done){
			var user = _.cloneDeep(fx.users.user0)
			user.email = email
			request(sails.hooks.http.app)
				.post('/users')
				.send(user)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.have.property("data");
					res.body.response.data.should.have.property("firstName");
					res.body.response.data.should.have.property("lastName");
					res.body.response.data.should.have.property("email");
					done()				
				});
		});

		it("should fail when email is not unique", function(done){
			var user = _.cloneDeep(fx.users.user0)
			user.email = email
			request(sails.hooks.http.app)
				.post('/users')
				.send(user)
				.expect(400)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.have.property("errors");
					res.body.response.errors.email.should.containEql({ rule: 'unique', message: 'Email is already registered' })
					done()				
				});
		});

	})

	describe("read()", function (){

		var testUser = null;

		before(function(done){
			var user = _.cloneDeep(fx.users.user0)
			user.email = randomstring.generate(5) + "@gmail.com";
			request(sails.hooks.http.app)
				.post('/users')
				.send(user)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					testUser = res.body.response.data
					done()
				})
		})

		it("should fail when a non-existing id is used", function(done){
			request(sails.hooks.http.app)
				.get('/users/0')
				.expect(404)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.message.should.be.equal("User not found")
					done()				
				});
		});

		it("should successfully retrieve a user", function(done){
			request(sails.hooks.http.app)
				.get('/users/' + testUser.id)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.not.have.property("errors")
					done()				
				});
		});
	})

	describe("list()", function (){


		it("should successfully retrieve a list of users", function(done){
			request(sails.hooks.http.app)
				.get('/users')
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.not.have.property("errors")
					res.body.response.should.have.property("data")
					res.body.response.data.should.be.an.Array;
					done()				
				});
		});

	})

	describe("update()", function (){

		var testUser = null;

		before(function(done){
			var user = _.cloneDeep(fx.users.user0)
			user.email = randomstring.generate(5) + "@gmail.com";
			request(sails.hooks.http.app)
				.post('/users')
				.send(user)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					testUser = res.body.response.data
					done()
				})
		})

		// this test indicates validation is enabled in updates ops
		it("should return error when email is updated with an invalid value", function(done){
			request(sails.hooks.http.app)
				.put('/users/' + testUser.id)
				.send({ email: "invalid.com" })
				.expect(400)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.have.property("errors");
					res.body.response.errors.should.have.property("email")
					res.body.response.errors.email.should.containEql({ rule: 'email', message: 'Email is invalid' })
					done()				
				});
		});

		it("should fail because the user id does not exist", function(done){
			var newEmail = "jane@gmail.com";
			request(sails.hooks.http.app)
				.put('/users/abcde')
				.send({ email: newEmail })
				.expect(404)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.message.should.be.equal("User not found");
					done()				
				});
		});

		it("should successfully update a field", function(done){
			var newEmail = "jane@gmail.com";
			request(sails.hooks.http.app)
				.put('/users/' + testUser.id)
				.send({ email: newEmail })
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.not.have.property("errors");
					res.body.response.should.have.property("data")
					res.body.response.data.email.should.be.equal(newEmail)
					res.body.response.data.should.be.an.Object;
					done()				
				});
		});		
	})

	describe("delete()", function (){

		var testUser = null;

		before(function(done){
			var user = _.cloneDeep(fx.users.user0)
			user.email = randomstring.generate(5) + "@gmail.com";
			request(sails.hooks.http.app)
				.post('/users')
				.send(user)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					testUser = res.body.response.data
					done()
				})
		})

		it("should successfully delete a user", function(done){
			request(sails.hooks.http.app)
				.delete('/users/' + testUser.id)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.not.have.property("errors");
					res.body.response.message.should.be.equal("User removed successfully")
					done()				
				});
		});

		it("should fail to delete a user because id does not exist", function(done){
			request(sails.hooks.http.app)
				.delete('/users/abcde')
				.expect(404)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.message.should.be.equal("User not found")
					done()				
				});
		});

	});

});