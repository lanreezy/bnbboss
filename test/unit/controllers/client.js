// User controller tests
var after = require('after'),
	should = require('should'),
	request = require('supertest'),
	randomstring = require('randomstring'),
	fx = require('node-fixtures');

describe('ClientController', function(){

	var testUser, token = null;

	before(function(done) {
		var user = _.cloneDeep(fx.users.user0)
		user.email = randomstring.generate(5) + "@gmail.com";
		request(sails.hooks.http.app)
			.post('/users')
			.send(user)
			.expect(200)
			.end(function(err, res){
				if (err) return done(err)
				testUser = res.body.response.data
				done()
			})
	})

	before(function(done) {
		request(sails.hooks.http.app)
			.post('/auth/login')
			.send({ email: testUser.email, password: fx.users.user0.password })
			.expect(200)
			.end(function(err, res){
				if (err) return done(err)
				token = res.body.response.data.token
				done()
			})
	})

	describe("create()", function (){

		it("should return multiple validation errors when empty request body is provided", function(done){
			request(sails.hooks.http.app)
				.post('/clients')
				.set("Authorization", "Bearer " + token)
				.send({})
				.expect(400)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.have.property("errors");
					res.body.response.errors.should.have.property("firstName")
					res.body.response.errors.should.have.property("lastName")
					res.body.response.errors.should.have.property("username")
					res.body.response.errors.should.have.property("airbnbEmail")
					res.body.response.errors.should.have.property("airbnbPassword")
					done()				
				});
		});

		it("should fail when airbnb email address is invalid", function(done){
			var client = _.cloneDeep(fx.clients.client0)
			client.airbnbEmail = randomstring.generate(5) + "gmail.com"
			request(sails.hooks.http.app)
				.post('/clients')
				.set("Authorization", "Bearer " + token)
				.send(client)
				.expect(400)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.have.property("errors");
					res.body.response.errors.should.have.property("airbnbEmail")
					res.body.response.errors.airbnbEmail.should.containEql({ rule: 'email', message: 'Airbnb email is invalid' })
					done()				
				});
		});

		it("should successfully create a new client", function(done){
			var client = _.cloneDeep(fx.clients.client0)
			request(sails.hooks.http.app)
				.post('/clients')
				.set("Authorization", "Bearer " + token)
				.send(client)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.have.property("data");
					res.body.response.data.should.have.property("firstName");
					res.body.response.data.should.have.property("lastName");
					res.body.response.data.should.have.property("username");
					res.body.response.data.should.have.property("airbnbEmail");
					res.body.response.data.should.have.property("airbnbPassword");
					done()				
				});
		});

		it("should fail because client username is not available", function(done){
			var client = _.cloneDeep(fx.clients.client0)
			request(sails.hooks.http.app)
				.post('/clients')
				.set("Authorization", "Bearer " + token)
				.send(client)
				.expect(400)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.have.property("errors");
					res.body.response.errors.should.have.property("username")
					res.body.response.errors.username.should.containEql({ rule: 'unique', message: 'Username is already registered' })
					done()				
				});
		});
	});

	describe("read()", function (){

		var testClient = null;

		before(function(done){
			var client = _.cloneDeep(fx.clients.client0)
			client.username = randomstring.generate(5);
			request(sails.hooks.http.app)
				.post('/clients')
				.set("Authorization", "Bearer " + token)
				.send(client)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					testClient = res.body.response.data
					done()
				})
		})

		it("should fail when a non-existing id is used", function(done){
			request(sails.hooks.http.app)
				.get('/clients/abcde')
				.expect(404)
				.set("Authorization", "Bearer " + token)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.message.should.be.equal("Client not found")
					done()				
				});
		});

		it("should successfully retrieve a client", function(done){
			request(sails.hooks.http.app)
				.get('/clients/' + testClient.id)
				.expect(200)
				.set("Authorization", "Bearer " + token)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.not.have.property("errors")
					done()				
				});
		});
	})

	describe("list()", function (){

		it("should successfully retrieve a list of clients", function(done){
			request(sails.hooks.http.app)
				.get('/clients')
				.set("Authorization", "Bearer " + token)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.not.have.property("errors")
					res.body.response.should.have.property("data")
					res.body.response.data.should.be.an.Array;
					done()				
				});
		});

	})

	describe("update()", function (){

		var testClient = null;

		before(function(done){
			var client = _.cloneDeep(fx.clients.client0)
			client.username = randomstring.generate(5);
			request(sails.hooks.http.app)
				.post('/clients')
				.set("Authorization", "Bearer " + token)
				.send(client)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					testClient = res.body.response.data
					done()
				})
		});

		// this test indicates validation is enabled in updates ops
		it("should return error when airbnb email is updated with an invalid value", function(done){
			request(sails.hooks.http.app)
				.put('/clients/' + testClient.id)
				.send({ airbnbEmail: "invalid.com" })
				.set("Authorization", "Bearer " + token)
				.expect(400)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.have.property("errors");
					res.body.response.errors.should.have.property("airbnbEmail")
					res.body.response.errors.airbnbEmail.should.containEql({ rule: 'email', message: 'Airbnb email is invalid' })
					done()				
				});
		});

		it("should fail because the client id does not exist", function(done){
			var newEmail = "jane@gmail.com";
			request(sails.hooks.http.app)
				.put('/clients/abcde')
				.set("Authorization", "Bearer " + token)
				.send({ email: newEmail })
				.expect(404)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.message.should.be.equal("Client not found");
					done()				
				});
		});

		it("should successfully update a field", function(done){
			var newEmail = "jane@gmail.com";
			request(sails.hooks.http.app)
				.put('/clients/' + testClient.id)
				.set("Authorization", "Bearer " + token)
				.send({ airbnbEmail: newEmail })
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.not.have.property("errors");
					res.body.response.should.have.property("data")
					res.body.response.data.airbnbEmail.should.be.equal(newEmail)
					res.body.response.data.should.be.an.Object;
					done()				
				});
		});		
	});

	describe("delete()", function (){

		var testClient = null;

		before(function(done){
			var client = _.cloneDeep(fx.clients.client0)
			client.username = randomstring.generate(5);
			request(sails.hooks.http.app)
				.post('/clients')
				.set("Authorization", "Bearer " + token)
				.send(client)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					testClient = res.body.response.data
					done()
				})
		});

		it("should successfully delete a client", function(done){
			request(sails.hooks.http.app)
				.delete('/clients/' + testClient.id)
				.set("Authorization", "Bearer " + token)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.not.have.property("errors");
					res.body.response.message.should.be.equal("Client removed successfully")
					done()				
				});
		});

		it("should fail to delete a client because id does not exist", function(done){
			request(sails.hooks.http.app)
				.delete('/clients/abcde')
				.set("Authorization", "Bearer " + token)
				.expect(404)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.message.should.be.equal("Client not found")
					done()				
				});
		});

	});


})