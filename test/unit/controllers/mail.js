// Mail controller tests
var after = require('after'),
	should = require('should'),
	request = require('supertest'),
	randomstring = require('randomstring'),
	fx = require('node-fixtures');

describe('MailController', function(){

	var testUser, token = null;

	before(function(done) {
		var user = _.cloneDeep(fx.users.user0)
		user.role = "admin"
		user.email = randomstring.generate(5) + "@gmail.com";
		request(sails.hooks.http.app)
			.post('/users')
			.send(user)
			.expect(200)
			.end(function(err, res){
				if (err) return done(err)
				testUser = res.body.response.data
				done()
			})
	})

	before(function(done) {
		request(sails.hooks.http.app)
			.post('/auth/login')
			.send({ email: testUser.email, password: fx.users.user0.password })
			.expect(200)
			.end(function(err, res){
				if (err) return done(err)
				token = res.body.response.data.token
				done()
			})
	})

	describe("create()", function (){

		it("should return multiple validation errors when empty request body is provided", function(done){
			request(sails.hooks.http.app)
				.post('/mails')
				.set("Authorization", "Bearer " + token)
				.send({})
				.expect(400)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.have.property("errors");
					res.body.response.errors.should.have.property("username")
					res.body.response.errors.should.have.property("from")
					res.body.response.errors.should.have.property("subject")
					res.body.response.errors.should.have.property("message")
					done()				
				});
		});

		it("should successfully create a new mail", function(done){
			var mail = _.cloneDeep(fx.mails.mail0)
			request(sails.hooks.http.app)
				.post('/mails')
				.set("Authorization", "Bearer " + token)
				.send(mail)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.have.property("data");
					res.body.response.data.should.have.property("username")
					res.body.response.data.should.have.property("from")
					res.body.response.data.should.have.property("subject")
					res.body.response.data.should.have.property("message")
					done()				
				});
		});
	})

	describe("read()", function (){

		var testMail = null;

		before(function(done){
			var mail = _.cloneDeep(fx.mails.mail0)
			request(sails.hooks.http.app)
				.post('/mails')
				.set("Authorization", "Bearer " + token)
				.send(mail)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					testMail = res.body.response.data
					done()
				})
		})

		it("should fail when a non-existing id is used", function(done){
			request(sails.hooks.http.app)
				.get('/mails/abcde')
				.set("Authorization", "Bearer " + token)
				.expect(404)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.message.should.be.equal("Mail not found")
					done()				
				});
		});

		it("should successfully retrieve a mail", function(done){
			request(sails.hooks.http.app)
				.get('/mails/' + testMail.id)
				.set("Authorization", "Bearer " + token)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.not.have.property("errors")
					done()				
				});
		});

	})

	describe("list()", function (){

		it("should successfully retrieve a list of mails", function(done){
			request(sails.hooks.http.app)
				.get('/mails')
				.set("Authorization", "Bearer " + token)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.not.have.property("errors")
					res.body.response.should.have.property("data")
					res.body.response.data.should.be.an.Array;
					done()				
				});
		});

	})

	describe("update()", function (){

		var testMail = null;

		before(function(done){
			var mail = _.cloneDeep(fx.mails.mail0)
			request(sails.hooks.http.app)
				.post('/mails')
				.set("Authorization", "Bearer " + token)
				.send(mail)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					testMail = res.body.response.data
					done()
				})
		});

		it("should fail because the mail id does not exist", function(done){
			request(sails.hooks.http.app)
				.put('/mails/abcde')
				.set("Authorization", "Bearer " + token)
				.send({})
				.expect(404)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.message.should.be.equal("Mail not found");
					done()				
				});
		});

		it("should successfully update a field", function(done){
			var newSubject = "Coming? Lets meet";
			request(sails.hooks.http.app)
				.put('/mails/' + testMail.id)
				.set("Authorization", "Bearer " + token)
				.send({ subject: newSubject })
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.not.have.property("errors");
					res.body.response.should.have.property("data")
					res.body.response.data.subject.should.be.equal(newSubject)
					res.body.response.data.should.be.an.Object;
					done()				
				});
		});

	})

	describe("delete()", function (){

		var testMail = null;

		before(function(done){
			var mail = _.cloneDeep(fx.mails.mail0)
			request(sails.hooks.http.app)
				.post('/mails')
				.set("Authorization", "Bearer " + token)
				.send(mail)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					testMail = res.body.response.data
					done()
				})
		});

		it("should successfully delete a mail", function(done){
			request(sails.hooks.http.app)
				.delete('/mails/' + testMail.id)
				.set("Authorization", "Bearer " + token)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.not.have.property("errors");
					res.body.response.message.should.be.equal("Mail removed successfully")
					done()				
				});
		});

		it("should fail to delete a mail because id does not exist", function(done){
			request(sails.hooks.http.app)
				.delete('/mails/abcde')
				.set("Authorization", "Bearer " + token)
				.expect(404)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.message.should.be.equal("Mail not found")
					done()				
				});
		});

	});
})