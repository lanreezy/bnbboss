// Notification controller tests
var after = require('after'),
	should = require('should'),
	request = require('supertest'),
	randomstring = require('randomstring'),
	fx = require('node-fixtures');

describe('NotificationController', function(){

	var testUser, token = null;

	before(function(done) {
		var user = _.cloneDeep(fx.users.user0)
		user.email = randomstring.generate(5) + "@gmail.com";
		request(sails.hooks.http.app)
			.post('/users')
			.send(user)
			.expect(200)
			.end(function(err, res){
				if (err) return done(err)
				testUser = res.body.response.data
				done()
			})
	})

	before(function(done) {
		request(sails.hooks.http.app)
			.post('/auth/login')
			.send({ email: testUser.email, password: fx.users.user0.password })
			.expect(200)
			.end(function(err, res){
				if (err) return done(err)
				token = res.body.response.data.token
				done()
			})
	})

	describe("create()", function (){

		it("should return multiple validation errors when empty request body is provided", function(done){
			request(sails.hooks.http.app)
				.post('/notifications')
				.set("Authorization", "Bearer " + token)
				.send({})
				.expect(400)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.have.property("errors");
					res.body.response.errors.should.have.property("client")
					res.body.response.errors.should.have.property("type")
					res.body.response.errors.should.have.property("from")
					res.body.response.errors.should.have.property("subject")
					res.body.response.errors.should.have.property("message")
					done()				
				});
		});

		it("should successfully create a new notification", function(done){
			var notification = _.cloneDeep(fx.notifications.notification0)
			request(sails.hooks.http.app)
				.post('/notifications')
				.set("Authorization", "Bearer " + token)
				.send(notification)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.have.property("data");
					res.body.response.data.should.have.property("client")
					res.body.response.data.should.have.property("type")
					res.body.response.data.should.have.property("from")
					res.body.response.data.should.have.property("subject")
					res.body.response.data.should.have.property("message")
					done()				
				});
		});

	})

	describe("read()", function (){

		var testNotification = null;

		before(function(done){
			var notification = _.cloneDeep(fx.notifications.notification0)
			request(sails.hooks.http.app)
				.post('/notifications')
				.set("Authorization", "Bearer " + token)
				.send(notification)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					testNotification = res.body.response.data
					done()
				})
		})

		it("should fail when a non-existing id is used", function(done){
			request(sails.hooks.http.app)
				.get('/notifications/abcde')
				.set("Authorization", "Bearer " + token)
				.expect(404)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.message.should.be.equal("Notification not found")
					done()				
				});
		});

		it("should successfully retrieve a notification", function(done){
			request(sails.hooks.http.app)
				.get('/notifications/' + testNotification.id)
				.set("Authorization", "Bearer " + token)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.not.have.property("errors")
					done()				
				});
		});

	})

	describe("list()", function (){

		it("should successfully retrieve a list of notifications", function(done){
			request(sails.hooks.http.app)
				.get('/notifications')
				.set("Authorization", "Bearer " + token)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.not.have.property("errors")
					res.body.response.should.have.property("data")
					res.body.response.data.should.be.an.Array;
					done()				
				});
		});

	})


	describe("update()", function (){

		var testNotification = null;

		before(function(done){
			var notification = _.cloneDeep(fx.notifications.notification0)
			request(sails.hooks.http.app)
				.post('/notifications')
				.set("Authorization", "Bearer " + token)
				.send(notification)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					testNotification = res.body.response.data
					done()
				})
		});

		it("should fail because the notification id does not exist", function(done){
			request(sails.hooks.http.app)
				.put('/notifications/abcde')
				.set("Authorization", "Bearer " + token)
				.send({})
				.expect(404)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.message.should.be.equal("Notification not found");
					done()				
				});
		});

		it("should successfully update a field", function(done){
			var newSubject = "Coming? Lets meet";
			request(sails.hooks.http.app)
				.put('/notifications/' + testNotification.id)
				.set("Authorization", "Bearer " + token)
				.send({ subject: newSubject })
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.not.have.property("errors");
					res.body.response.should.have.property("data")
					res.body.response.data.subject.should.be.equal(newSubject)
					res.body.response.data.should.be.an.Object;
					done()				
				});
		});

	})

	describe("delete()", function (){

		var testNotification = null;

		before(function(done){
			var notification = _.cloneDeep(fx.notifications.notification0)
			request(sails.hooks.http.app)
				.post('/notifications')
				.set("Authorization", "Bearer " + token)
				.send(notification)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					testNotification = res.body.response.data
					done()
				})
		});

		it("should successfully delete a notification", function(done){
			request(sails.hooks.http.app)
				.delete('/notifications/' + testNotification.id)
				.set("Authorization", "Bearer " + token)
				.expect(200)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.should.not.have.property("errors");
					res.body.response.message.should.be.equal("Notification removed successfully")
					done()				
				});
		});

		it("should fail to delete a notification because id does not exist", function(done){
			request(sails.hooks.http.app)
				.delete('/notifications/abcde')
				.set("Authorization", "Bearer " + token)
				.expect(404)
				.end(function(err, res){
					if (err) return done(err)
					res.body.should.be.an.Object;
					res.body.should.have.property("response");
					res.body.response.message.should.be.equal("Notification not found")
					done()				
				});
		});

	});

})


