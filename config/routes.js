/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

    /***************************************************************************
     *                                                                          *
     * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
     * etc. depending on your default view engine) your home page.              *
     *                                                                          *
     * (Alternatively, remove this and add an `index.html` file in your         *
     * `assets` directory)                                                      *
     *                                                                          *
     ***************************************************************************/

    '/': {
        view: 'homepage'
    },

    /**
     * Auth routes
     */
    'POST /auth/login': {
        controller: 'AuthController',
        action: 'login'
    },

    
    /*
     * User Controller Routes
     */
    'GET /admin/setup': {
        controller: 'UserController',
        action: 'setup'
    },

    'POST /users': {
        controller: 'UserController',
        action: 'create'
    },

    'GET /users': {
        controller: 'UserController',
        action: 'list'
    },
    'GET /users/:id': {
        controller: 'UserController',
        action: 'read'
    },
    'PUT /users/:id': {
        controller: 'UserController',
        action: 'update'
    },
    'DELETE /users/:id': {
        controller: 'UserController',
        action: 'delete'
    },

    /**
     * Client routes
     */
    'POST /clients': {
        controller: 'ClientController',
        action: 'create'
    },
    'GET /clients/:id': {
        controller: 'ClientController',
        action: 'read'
    },
    'GET /clients': {
        controller: 'ClientController',
        action: 'list'
    },
    'PUT /clients/:id': {
        controller: 'ClientController',
        action: 'update'
    },
    'DELETE /clients/:id': {
        controller: 'ClientController',
        action: 'delete'
    },

    /**
     * Notification routes
     */
    'POST /notifications': {
        controller: 'NotificationController',
        action: 'create'
    },
    'GET /notifications/:id': {
        controller: 'NotificationController',
        action: 'read'
    },
    'GET /notifications': {
        controller: 'NotificationController',
        action: 'list'
    },
    'PUT /notifications/:id': {
        controller: 'NotificationController',
        action: 'update'
    },
    'DELETE /notifications/:id': {
        controller: 'NotificationController',
        action: 'delete'
    },

    /**
     * Mail routes
     */
    'POST /mails': {
        controller: 'MailController',
        action: 'create'
    },
    'GET /mails/:id': {
        controller: 'MailController',
        action: 'read'
    },
    'GET /mails': {
        controller: 'MailController',
        action: 'list'
    },
    'PUT /mails/:id': {
        controller: 'MailController',
        action: 'update'
    },
    'DELETE /mails/:id': {
        controller: 'MailController',
        action: 'delete'
    }


    /***************************************************************************
     *                                                                          *
     * Custom routes here...                                                    *
     *                                                                          *
     *  If a request to a URL doesn't match any of the custom routes above, it  *
     * is matched against Sails route blueprints. See `config/blueprints.js`    *
     * for configuration options and examples.                                  *
     *                                                                          *
     ***************************************************************************/

};
