// Settings

var moment = require('moment');

module.exports.settings = {

    // admin Email
    adminEmail: process.env.ADMIN_EMAIL,

    // admin Password
    adminPassword: process.env.ADMIN_PASSWORD,

    // base url
    baseURL:  process.env.BASE_URL || "bnbboss.com",

    // email domain
    emailDomain: process.env.EMAIL_DOMAIN,

    // sparkpost key
    sparkPostKey: process.env.SPARKPOST_API_KEY,

    // jwt secret
    jwtSecret: "xxxxxxxxxxxxxxxxx",

    // jwt expiry time 
    tokenExpiryInSeconds: 31536000,

    // embeded smtp server port
    smtpServerPort: 25,

    // list of usernames reserver for admin or non-client useage
    reservedNames: ["info"]
}